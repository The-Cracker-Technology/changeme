rm -rf /opt/ANDRAX/changeme

python3 -m venv /opt/ANDRAX/changeme

source /opt/ANDRAX/changeme/bin/activate

/opt/ANDRAX/changeme/bin/pip3 install wheel

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install wheel... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/changeme/bin/pip3 install -r requirements.txt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install requirements... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/changeme/package

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf changeme.1 /usr/local/share/man/man1/
chmod 644 /usr/local/share/man/man1/changeme.1
mandb

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
